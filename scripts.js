const cards = document.querySelectorAll('.memory-card')

let hasFlippedCard = false,
  firstCard, secondCard,
  lockBoard = false;

function flipCard() {
  if (lockBoard) {return;}
  if (this === firstCard) {return;}

  this.classList.add('flip')

  if (!hasFlippedCard) {
    hasFlippedCard = true;
    firstCard = this;
    return
  }

  secondCard = this;
  hasFlippedCard = false;

  checkForMatch();
}

function checkForMatch() {
  firstCard.dataset.name === secondCard.dataset.name ? disableCard() : unflipCards();
}

function disableCard() {
  firstCard.removeEventListener('click', flipCard)
  secondCard.removeEventListener('click', flipCard)
  resetBoard()
}

function unflipCards() {
  lockBoard = true;

  setTimeout(() => {
    firstCard.classList.remove('flip')
    secondCard.classList.remove('flip')
    lockBoard = false;

    resetBoard()
  }, 1500)

}

function resetBoard() {
  hasFlippedCard =  lockBoard = false;
  firstCard = secondCard = null
}

cards.forEach(card => {
  const ramdomPos = Math.floor(Math.random() * cards.length);
  card.style.order = ramdomPos;
})

cards.forEach(card => card.addEventListener('click', flipCard))